
#include <iostream>
#include <map>       // dict from node pairs to arcs
#include <algorithm> // sort
#include <utility>

#include <lemon/list_graph.h>
#include <lemon/smart_graph.h>
#include <lemon/static_graph.h>

#include <lemon/network_simplex.h>
#include <lemon/cost_scaling.h>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

using namespace lemon;
using namespace std;

namespace lens {

  // GRAPH WRAPPER

  // Lemon graph wrapper class

  template <class G>
  struct GraphWrap {
    G g;
    int nodes = 0;
    int arcs = 0;

    // determine the arc given two nodes
    map<pair<int, int>, int> dict;
    int addNode();
    void addNodes(int);
    int addArc(int, int);
    void addArcs(py::array_t<int>, py::array_t<int>);

    int getArc(int i, int j);

    int validNode(int i) {return 0 <= i && i < nodes;};
    int validArc(int i) {return 0 <= i && i < arcs;};

    void checkNode(int i) {if (!validNode(i)) throw std::out_of_range("node does not exist");};
    void checkArc(int i) {if (!validArc(i)) throw std::out_of_range("arc does not exist");};

    int nNodes() {return nodes;}
    int nArcs() {return arcs;}

    GraphWrap() = default;
    GraphWrap(int n);
    GraphWrap(int, py::array_t<int>, py::array_t<int>);
  };

  template <class G>
  GraphWrap<G>::GraphWrap(int n) {
    addNodes(n); 
  }

  template <class G>
  GraphWrap<G>::GraphWrap(int n,
                          py::array_t<int> sources,
                          py::array_t<int> targets) {
    addNodes(n);
    addArcs(sources, targets);
  }

  template <class G>
  int GraphWrap<G>::addNode() {
    auto n = g.addNode();
    int id = g.id(n);
    assert(id >= 0 && id <= nodes);
    if (id == nodes) {
      nodes++;
    }

    return id;
  }

  template <class G>
  void GraphWrap<G>::addNodes(int n) {
    for (int i = 0; i < n; i++)
      addNode();
  }

  template <class G>
  int GraphWrap<G>::addArc(int i, int j) {
    checkNode(i);
    checkNode(j);
    auto s = g.nodeFromId(i);
    auto t = g.nodeFromId(j);
    auto arc = g.addArc(s, t);
    auto id = g.id(arc);
//    assert(id >= 0 && id <= arcs);
//  lemon seems to always yield new ids, even if the same arc already exists
    assert(id == arcs);
    dict[pair<int, int>(i, j)] = id;
    arcs++;

    return id;
  }

  template <class G>
  void GraphWrap<G>::addArcs(py::array_t<int> sources, 
                             py::array_t<int> targets) {

    py::size_t n = py::len(sources);
    if (n != py::len(targets))
      throw invalid_argument("source and target node id arrays must have same length");

    auto s = sources.unchecked<1>();
    auto t = targets.unchecked<1>();
    for (py::size_t i = 0; i < n; i++)
      addArc(s(i), t(i));
  }

  template <class G>
  int GraphWrap<G>::getArc(int i, int j) {
    try {
      return dict.at(pair<int, int>(i, j));
    }
    catch (...) {
      throw std::out_of_range("no arc between nodes");
    }
  }

  template <class G>
  using GraphWrapPtr = shared_ptr<GraphWrap<G>>;

  // NODE MAPS

  template <class G, typename F>
  struct NodeMapWrap {
    typename G :: template NodeMap<F> map;
    GraphWrapPtr<G> ptr;

    void set(int, F);
    F get(int);

    NodeMapWrap<G, F>(GraphWrapPtr<G> ptr) : map(ptr->g), ptr(ptr) {};
    NodeMapWrap<G, F>(GraphWrapPtr<G> ptr, py::array_t<F> vals) : map(ptr->g), ptr(ptr) {
      if ((int) py::len(vals) != ptr->nodes) {
        throw invalid_argument("number of node map values does not fit graph");
      }
      auto v = vals.unchecked();
      for (int i = 0; i < ptr->nodes; i++) set(i, v(i));
    };
  };

  template <class G, typename F>
  void NodeMapWrap<G, F>::set(int nodeid, F value) {
    ptr->checkNode(nodeid);
    auto node = ptr->g.nodeFromId(nodeid);
    map[node] = value;
  }

  template <class G, typename F>
  F NodeMapWrap<G, F>::get(int nodeid) {
    ptr->checkNode(nodeid);
    auto node = ptr->g.nodeFromId(nodeid);
    return map[node];
  }

  // ARC MAPS

  template <class G, typename F>
  struct ArcMapWrap {
    typename G :: template ArcMap<F> map;
    GraphWrapPtr<G> ptr;

    void set(int, F);
    void set (int, int, F);

    F get(int);
    F get(int, int);

    ArcMapWrap<G, F>(GraphWrapPtr<G> ptr) : map(ptr->g), ptr(ptr) {};
    ArcMapWrap<G, F>(GraphWrapPtr<G> ptr, py::array_t<F> vals) : map(ptr->g), ptr(ptr) {
      if ((int) py::len(vals) != ptr->arcs) {
        throw invalid_argument("number of arc map values does not fit graph");
      }
      auto v = vals.unchecked();
      for (int i = 0; i < ptr->arcs; i++) set(i, v(i));
    };
  };

  template <class G, typename F>
  void ArcMapWrap<G, F>::set(int arcid, F value) {
    ptr->checkArc(arcid);
    auto arc = ptr->g.arcFromId(arcid);
    map[arc] = value;
  }

  template <class G, typename F>
  void ArcMapWrap<G, F>::set(int source, int target, F value) {
    ptr->checkNode(source);
    ptr->checkNode(target);
    set(ptr->getArc(source, target), value);
  }

  template <class G, typename F>
  F ArcMapWrap<G, F>::get(int arcid) {
    ptr->checkArc(arcid);
    auto arc = ptr->g.arcFromId(arcid);
    return map[arc];
  }

  template <class G, typename F>
  F ArcMapWrap<G, F>::get(int source, int target) {
    ptr->checkNode(source);
    ptr->checkNode(target);
    return get(ptr->getArc(source, target));
  }

  // NETWORK SIMPLEX

  // Network Simplex solver
  
  template <class G, typename F>
  struct NetworkSimplexWrap {
    NetworkSimplex<G, F> solver;
    GraphWrapPtr<G> ptr;

    shared_ptr<NodeMapWrap<G, F>> supply_ptr;
    shared_ptr<ArcMapWrap<G, F>> cost_ptr;
    shared_ptr<ArcMapWrap<G, F>> lower_ptr;
    shared_ptr<ArcMapWrap<G, F>> upper_ptr;

    void supplyMap(shared_ptr<NodeMapWrap<G, F>> supply){
      supply_ptr = supply;
      solver.supplyMap(supply->map);
    }

    void costMap(shared_ptr<ArcMapWrap<G, F>> cost) {
      cost_ptr = cost;
      solver.costMap(cost->map);
    }

    void upperMap(shared_ptr<ArcMapWrap<G, F>> upper) {
      upper_ptr = upper;
      solver.upperMap(upper->map);
    }
    void lowerMap(shared_ptr<ArcMapWrap<G, F>> lower) {
      lower_ptr = lower;
      solver.lowerMap(lower->map);
    }

    string run();
    F totalCost() { return solver.totalCost(); };
    F flow(int arc_id);
    F flow(int source, int target);

    NetworkSimplexWrap<G, F>(GraphWrapPtr<G> ptr,
                      shared_ptr<NodeMapWrap<G, F>> supply,
                      shared_ptr<ArcMapWrap<G, F>> cost,
                      shared_ptr<ArcMapWrap<G, F>> lower,
                      shared_ptr<ArcMapWrap<G, F>> upper)
                      : solver(ptr->g), ptr(ptr) {

      if (supply) supplyMap(supply);
      if (cost)   costMap(cost);
      if (lower)  lowerMap(lower);
      if (upper)  upperMap(upper);
    }
  };

  // Run and evaluate the network simplex

  template <class G, typename F>
  string NetworkSimplexWrap<G, F>::run() {
    auto ret = solver.run();
    switch(ret) {
      case NetworkSimplex<G, F>::ProblemType::INFEASIBLE : return string("infeasible");
      case NetworkSimplex<G, F>::ProblemType::UNBOUNDED : return string("unbounded");
      case NetworkSimplex<G, F>::ProblemType::OPTIMAL : return string("optimal");
      default: throw runtime_error("unknown problem type");
    }
  }

  template <class G, typename F>
  F NetworkSimplexWrap<G, F>::flow(int arcid) {
    ptr->checkArc(arcid);
    auto arc = ptr->g.arcFromId(arcid);
    return solver.flow(arc);
  }

  template <class G, typename F>
  F NetworkSimplexWrap<G, F>::flow(int source, int target) {
    ptr->checkNode(source);
    ptr->checkNode(target);
    return flow(ptr->getArc(source, target));
  }

  // NETWORK SIMPLEX

  // Network Simplex solver
  
  template <class G, typename F>
  struct CostScalingWrap {
    CostScaling<G, F> solver;
    GraphWrapPtr<G> ptr;

    shared_ptr<NodeMapWrap<G, F>> supply_ptr;
    shared_ptr<ArcMapWrap<G, F>> cost_ptr;
    shared_ptr<ArcMapWrap<G, F>> lower_ptr;
    shared_ptr<ArcMapWrap<G, F>> upper_ptr;

    void supplyMap(shared_ptr<NodeMapWrap<G, F>> supply){
      supply_ptr = supply;
      solver.supplyMap(supply->map);
    }

    void costMap(shared_ptr<ArcMapWrap<G, F>> cost) {
      cost_ptr = cost;
      solver.costMap(cost->map);
    }

    void upperMap(shared_ptr<ArcMapWrap<G, F>> upper) {
      upper_ptr = upper;
      solver.upperMap(upper->map);
    }
    void lowerMap(shared_ptr<ArcMapWrap<G, F>> lower) {
      lower_ptr = lower;
      solver.lowerMap(lower->map);
    }

    string run();
    F totalCost() { return solver.totalCost(); };
    F flow(int arc_id);
    F flow(int source, int target);

    CostScalingWrap<G, F>(GraphWrapPtr<G> ptr,
                      shared_ptr<NodeMapWrap<G, F>> supply,
                      shared_ptr<ArcMapWrap<G, F>> cost,
                      shared_ptr<ArcMapWrap<G, F>> lower,
                      shared_ptr<ArcMapWrap<G, F>> upper)
                      : solver(ptr->g), ptr(ptr) {

      if (supply) supplyMap(supply);
      if (cost)   costMap(cost);
      if (lower)  lowerMap(lower);
      if (upper)  upperMap(upper);
    }
  };

  // Run and evaluate the network simplex

  template <class G, typename F>
  string CostScalingWrap<G, F>::run() {
    auto ret = solver.run();
    switch(ret) {
      case CostScaling<G, F>::ProblemType::INFEASIBLE : return string("infeasible");
      case CostScaling<G, F>::ProblemType::UNBOUNDED : return string("unbounded");
      case CostScaling<G, F>::ProblemType::OPTIMAL : return string("optimal");
      default: throw runtime_error("unknown problem type");
    }
  }

  template <class G, typename F>
  F CostScalingWrap<G, F>::flow(int arcid) {
    ptr->checkArc(arcid);
    auto arc = ptr->g.arcFromId(arcid);
    return solver.flow(arc);
  }

  template <class G, typename F>
  F CostScalingWrap<G, F>::flow(int source, int target) {
    ptr->checkNode(source);
    ptr->checkNode(target);
    return flow(ptr->getArc(source, target));
  }



  // STATIC GRAPH SPECIALIZATION

  // Specific methods for the StaticGraph, which does not support adding nodes
  // or arcs
  template<>
  int GraphWrap<StaticDigraph>::addNode() {
    throw runtime_error("addNode() not defined for StaticDigraph");
  }

  template<>
  void GraphWrap<StaticDigraph>::addNodes(int) {
    throw runtime_error("addNodes() not implemented for StaticDigraph");
  }

  template<>
  int GraphWrap<StaticDigraph>::addArc(int, int) {
    throw runtime_error("addArc() not implemented for StaticDigraph");
  }

  template<>
  void GraphWrap<StaticDigraph>::addArcs(py::array_t<int>, py::array_t<int>) {
    throw runtime_error("addArcs() not implemented for StaticDigraph");
  }

  template<>
  GraphWrap<StaticDigraph>::GraphWrap(int n) {
    throw runtime_error("constructor StaticDigraph(int) not imlemented");
  }

  template <>
  GraphWrap<StaticDigraph>::GraphWrap(int n,
                                      py::array_t<int> sources,
                                      py::array_t<int> targets) {

    py::size_t n_arcs = py::len(sources);
    if (n_arcs != py::len(targets))
      throw invalid_argument("source and target node id arrays must have same length");

    auto s = sources.unchecked<1>();
    auto t = targets.unchecked<1>();
    vector<pair<int, int>> pairs;
    int prev_s = 0;
    for (py::size_t i = 0; i < n_arcs; i++) {
      int si = s(i);
      int ti = t(i);
      // consistency checks
      if (si < prev_s)
        throw std::invalid_argument("source nodes must be ordered");
      if (si < 0 || ti < 0 || si >= n || ti >= n)
        throw std::out_of_range("node out of range");
      // fill containers
      pair<int, int> p(si, ti);
      // attention: lemon allows multiple arcs between the same nodes
      // we only save the latest one in dict, so only this latest one
      // can be retrieved when specifiny source and target instead of
      // arc id
      pairs.push_back(p);
      dict[p] = arcs++; // increment the arcs variable of the wrapper
      prev_s = si;
    }

    // construct the static graph
    g.build(n, pairs.begin(), pairs.end());

    // Set the nodes variable of the wrapper
    nodes = n;
  }

}

using namespace lens;
using namespace pybind11::literals;

using F = int;

PYBIND11_MODULE(lens, m) {
  m.doc() = "low level bindings to the network simplex implementation of the lemon optimization framework";

  // List digraph submodule

  py::module list_digraph = m.def_submodule("list_digraph", "provides the ListDigraph type");

  py::class_<GraphWrap<ListDigraph>, GraphWrapPtr<ListDigraph>>(list_digraph, "Graph")
    .def(py::init<>())
    .def(py::init<int>())
    .def(py::init<int, py::array_t<int>, py::array_t<int>>())
    .def("addNode", &GraphWrap<ListDigraph>::addNode)
    .def("addNodes", &GraphWrap<ListDigraph>::addNodes)
    .def("addArc", &GraphWrap<ListDigraph>::addArc)
    .def("addArcs", &GraphWrap<ListDigraph>::addArcs)
    .def("getArc", &GraphWrap<ListDigraph>::getArc)
    .def("nNodes", &GraphWrap<ListDigraph>::nNodes)
    .def("nArcs", &GraphWrap<ListDigraph>::nArcs);

  py::class_<NodeMapWrap<ListDigraph, F>,
             shared_ptr<NodeMapWrap<ListDigraph, F>>>
            (list_digraph, "NodeMap")
    .def(py::init<GraphWrapPtr<ListDigraph>>())
    .def(py::init<GraphWrapPtr<ListDigraph>, py::array_t<int>>())
    .def("set", &NodeMapWrap<ListDigraph, F>::set)
    .def("get", &NodeMapWrap<ListDigraph, F>::get);

  py::class_<ArcMapWrap<ListDigraph, F>,
             shared_ptr<ArcMapWrap<ListDigraph, F>>>
            (list_digraph, "ArcMap")
    .def(py::init<GraphWrapPtr<ListDigraph>>())
    .def(py::init<GraphWrapPtr<ListDigraph>, py::array_t<int>>())
    .def("set", py::overload_cast<int, F>(&ArcMapWrap<ListDigraph, F>::set))
    .def("set", py::overload_cast<int, int, F>(&ArcMapWrap<ListDigraph, F>::set))
    .def("get", py::overload_cast<int>(&ArcMapWrap<ListDigraph, F>::get))
    .def("get", py::overload_cast<int, int>(&ArcMapWrap<ListDigraph, F>::get));

  py::class_<NetworkSimplexWrap<ListDigraph, F>>(list_digraph, "NetworkSimplex")
    .def(py::init<GraphWrapPtr<ListDigraph>,
                  shared_ptr<NodeMapWrap<ListDigraph, F>>,
                  shared_ptr<ArcMapWrap<ListDigraph, F>>,
                  shared_ptr<ArcMapWrap<ListDigraph, F>>,
                  shared_ptr<ArcMapWrap<ListDigraph, F>>>(),
        "graph"_a, "supply"_a = nullptr, "cost"_a = nullptr,
        "lower"_a = nullptr, "upper"_a = nullptr)
    .def("costMap", &NetworkSimplexWrap<ListDigraph, F>::costMap)
    .def("supplyMap", &NetworkSimplexWrap<ListDigraph, F>::supplyMap)
    .def("lowerMap", &NetworkSimplexWrap<ListDigraph, F>::lowerMap)
    .def("upperMap", &NetworkSimplexWrap<ListDigraph, F>::upperMap)
    .def("run", &NetworkSimplexWrap<ListDigraph, F>::run)
    .def("totalCost", &NetworkSimplexWrap<ListDigraph, F>::totalCost)
    .def("flow", py::overload_cast<int>(&NetworkSimplexWrap<ListDigraph, F>::flow))
    .def("flow", py::overload_cast<int, int>(&NetworkSimplexWrap<ListDigraph, F>::flow));

  py::class_<CostScalingWrap<ListDigraph, F>>(list_digraph, "CostScaling")
    .def(py::init<GraphWrapPtr<ListDigraph>,
                  shared_ptr<NodeMapWrap<ListDigraph, F>>,
                  shared_ptr<ArcMapWrap<ListDigraph, F>>,
                  shared_ptr<ArcMapWrap<ListDigraph, F>>,
                  shared_ptr<ArcMapWrap<ListDigraph, F>>>(),
        "graph"_a, "supply"_a = nullptr, "cost"_a = nullptr,
        "lower"_a = nullptr, "upper"_a = nullptr)
    .def("costMap", &CostScalingWrap<ListDigraph, F>::costMap)
    .def("supplyMap", &CostScalingWrap<ListDigraph, F>::supplyMap)
    .def("lowerMap", &CostScalingWrap<ListDigraph, F>::lowerMap)
    .def("upperMap", &CostScalingWrap<ListDigraph, F>::upperMap)
    .def("run", &CostScalingWrap<ListDigraph, F>::run)
    .def("totalCost", &CostScalingWrap<ListDigraph, F>::totalCost)
    .def("flow", py::overload_cast<int>(&CostScalingWrap<ListDigraph, F>::flow))
    .def("flow", py::overload_cast<int, int>(&CostScalingWrap<ListDigraph, F>::flow));

  // Smart digraph submodule

  py::module smart_digraph = m.def_submodule("smart_digraph", "provides the SmartDigraph type");

  py::class_<GraphWrap<SmartDigraph>, GraphWrapPtr<SmartDigraph>>(smart_digraph, "Graph")
    .def(py::init<>())
    .def(py::init<int>())
    .def(py::init<int, py::array_t<int>, py::array_t<int>>())
    .def("addNode", &GraphWrap<SmartDigraph>::addNode)
    .def("addNodes", &GraphWrap<SmartDigraph>::addNodes)
    .def("addArc", &GraphWrap<SmartDigraph>::addArc)
    .def("addArcs", &GraphWrap<SmartDigraph>::addArcs)
    .def("getArc", &GraphWrap<SmartDigraph>::getArc)
    .def("nNodes", &GraphWrap<SmartDigraph>::nNodes)
    .def("nArcs", &GraphWrap<SmartDigraph>::nArcs);

  py::class_<NodeMapWrap<SmartDigraph, F>,
             shared_ptr<NodeMapWrap<SmartDigraph, F>>>
            (smart_digraph, "NodeMap")
    .def(py::init<GraphWrapPtr<SmartDigraph>>())
    .def(py::init<GraphWrapPtr<SmartDigraph>, py::array_t<int>>())
    .def("set", &NodeMapWrap<SmartDigraph, F>::set)
    .def("get", &NodeMapWrap<SmartDigraph, F>::get);


  py::class_<ArcMapWrap<SmartDigraph, F>,
             shared_ptr<ArcMapWrap<SmartDigraph, F>>>
            (smart_digraph, "ArcMap")
    .def(py::init<GraphWrapPtr<SmartDigraph>>())
    .def(py::init<GraphWrapPtr<SmartDigraph>, py::array_t<int>>())
    .def("set", py::overload_cast<int, F>(&ArcMapWrap<SmartDigraph, F>::set))
    .def("set", py::overload_cast<int, int, F>(&ArcMapWrap<SmartDigraph, F>::set))
    .def("get", py::overload_cast<int>(&ArcMapWrap<SmartDigraph, F>::get))
    .def("get", py::overload_cast<int, int>(&ArcMapWrap<SmartDigraph, F>::get));

  py::class_<NetworkSimplexWrap<SmartDigraph, F>>(smart_digraph, "NetworkSimplex")
    .def(py::init<GraphWrapPtr<SmartDigraph>,
                  shared_ptr<NodeMapWrap<SmartDigraph, F>>,
                  shared_ptr<ArcMapWrap<SmartDigraph, F>>,
                  shared_ptr<ArcMapWrap<SmartDigraph, F>>,
                  shared_ptr<ArcMapWrap<SmartDigraph, F>>>(),
        "graph"_a, "supply"_a = nullptr, "cost"_a = nullptr,
        "lower"_a = nullptr, "upper"_a = nullptr)
    .def("costMap", &NetworkSimplexWrap<SmartDigraph, F>::costMap)
    .def("supplyMap", &NetworkSimplexWrap<SmartDigraph, F>::supplyMap)
    .def("lowerMap", &NetworkSimplexWrap<SmartDigraph, F>::lowerMap)
    .def("upperMap", &NetworkSimplexWrap<SmartDigraph, F>::upperMap)
    .def("run", &NetworkSimplexWrap<SmartDigraph, F>::run)
    .def("totalCost", &NetworkSimplexWrap<SmartDigraph, F>::totalCost)
    .def("flow", py::overload_cast<int>(&NetworkSimplexWrap<SmartDigraph, F>::flow))
    .def("flow", py::overload_cast<int, int>(&NetworkSimplexWrap<SmartDigraph, F>::flow));

  py::class_<CostScalingWrap<SmartDigraph, F>>(smart_digraph, "CostScaling")
    .def(py::init<GraphWrapPtr<SmartDigraph>,
                  shared_ptr<NodeMapWrap<SmartDigraph, F>>,
                  shared_ptr<ArcMapWrap<SmartDigraph, F>>,
                  shared_ptr<ArcMapWrap<SmartDigraph, F>>,
                  shared_ptr<ArcMapWrap<SmartDigraph, F>>>(),
        "graph"_a, "supply"_a = nullptr, "cost"_a = nullptr,
        "lower"_a = nullptr, "upper"_a = nullptr)
    .def("costMap", &CostScalingWrap<SmartDigraph, F>::costMap)
    .def("supplyMap", &CostScalingWrap<SmartDigraph, F>::supplyMap)
    .def("lowerMap", &CostScalingWrap<SmartDigraph, F>::lowerMap)
    .def("upperMap", &CostScalingWrap<SmartDigraph, F>::upperMap)
    .def("run", &CostScalingWrap<SmartDigraph, F>::run)
    .def("totalCost", &CostScalingWrap<SmartDigraph, F>::totalCost)
    .def("flow", py::overload_cast<int>(&CostScalingWrap<SmartDigraph, F>::flow))
    .def("flow", py::overload_cast<int, int>(&CostScalingWrap<SmartDigraph, F>::flow));


  // Static digraph submodule

  py::module static_digraph = m.def_submodule("static_digraph", "provides the StaticDigraph type");

  py::class_<GraphWrap<StaticDigraph>, GraphWrapPtr<StaticDigraph>>(static_digraph, "Graph")
    .def(py::init<>())
    .def(py::init<int, py::array_t<int>, py::array_t<int>>())
    .def("getArc", &GraphWrap<StaticDigraph>::getArc)
    .def("nNodes", &GraphWrap<StaticDigraph>::nNodes)
    .def("nArcs", &GraphWrap<StaticDigraph>::nArcs);

  py::class_<NodeMapWrap<StaticDigraph, F>,
             shared_ptr<NodeMapWrap<StaticDigraph, F>>>
            (static_digraph, "NodeMap")
    .def(py::init<GraphWrapPtr<StaticDigraph>>())
    .def(py::init<GraphWrapPtr<StaticDigraph>, py::array_t<int>>())
    .def("set", &NodeMapWrap<StaticDigraph, F>::set)
    .def("get", &NodeMapWrap<StaticDigraph, F>::get);

  py::class_<ArcMapWrap<StaticDigraph, F>,
             shared_ptr<ArcMapWrap<StaticDigraph, F>>>
            (static_digraph, "ArcMap")
    .def(py::init<GraphWrapPtr<StaticDigraph>>())
    .def(py::init<GraphWrapPtr<StaticDigraph>, py::array_t<int>>())
    .def("set", py::overload_cast<int, F>(&ArcMapWrap<StaticDigraph, F>::set))
    .def("set", py::overload_cast<int, int, F>(&ArcMapWrap<StaticDigraph, F>::set))
    .def("get", py::overload_cast<int>(&ArcMapWrap<StaticDigraph, F>::get))
    .def("get", py::overload_cast<int, int>(&ArcMapWrap<StaticDigraph, F>::get));

  py::class_<NetworkSimplexWrap<StaticDigraph, F>>(static_digraph, "NetworkSimplex")
    .def(py::init<GraphWrapPtr<StaticDigraph>,
                  shared_ptr<NodeMapWrap<StaticDigraph, F>>,
                  shared_ptr<ArcMapWrap<StaticDigraph, F>>,
                  shared_ptr<ArcMapWrap<StaticDigraph, F>>,
                  shared_ptr<ArcMapWrap<StaticDigraph, F>>>(),
        "graph"_a, "supply"_a = nullptr, "cost"_a = nullptr,
        "lower"_a = nullptr, "upper"_a = nullptr)
    .def("costMap", &NetworkSimplexWrap<StaticDigraph, F>::costMap)
    .def("supplyMap", &NetworkSimplexWrap<StaticDigraph, F>::supplyMap)
    .def("lowerMap", &NetworkSimplexWrap<StaticDigraph, F>::lowerMap)
    .def("upperMap", &NetworkSimplexWrap<StaticDigraph, F>::upperMap)
    .def("run", &NetworkSimplexWrap<StaticDigraph, F>::run)
    .def("totalCost", &NetworkSimplexWrap<StaticDigraph, F>::totalCost)
    .def("flow", py::overload_cast<int>(&NetworkSimplexWrap<StaticDigraph, F>::flow))
    .def("flow", py::overload_cast<int, int>(&NetworkSimplexWrap<StaticDigraph, F>::flow));

  py::class_<CostScalingWrap<StaticDigraph, F>>(static_digraph, "CostScaling")
    .def(py::init<GraphWrapPtr<StaticDigraph>,
                  shared_ptr<NodeMapWrap<StaticDigraph, F>>,
                  shared_ptr<ArcMapWrap<StaticDigraph, F>>,
                  shared_ptr<ArcMapWrap<StaticDigraph, F>>,
                  shared_ptr<ArcMapWrap<StaticDigraph, F>>>(),
        "graph"_a, "supply"_a = nullptr, "cost"_a = nullptr,
        "lower"_a = nullptr, "upper"_a = nullptr)
    .def("costMap", &CostScalingWrap<StaticDigraph, F>::costMap)
    .def("supplyMap", &CostScalingWrap<StaticDigraph, F>::supplyMap)
    .def("lowerMap", &CostScalingWrap<StaticDigraph, F>::lowerMap)
    .def("upperMap", &CostScalingWrap<StaticDigraph, F>::upperMap)
    .def("run", &CostScalingWrap<StaticDigraph, F>::run)
    .def("totalCost", &CostScalingWrap<StaticDigraph, F>::totalCost)
    .def("flow", py::overload_cast<int>(&CostScalingWrap<StaticDigraph, F>::flow))
    .def("flow", py::overload_cast<int, int>(&CostScalingWrap<StaticDigraph, F>::flow));

}

