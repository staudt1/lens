
LEMON_INCLUDE=/opt/lemon/include
LEMON_LIBRARY=/opt/lemon/lib

lens: lens.cc
	c++ -O3 -Wall -shared -std=c++14 -fPIC $$(python3 -m pybind11 --includes) -I $(LEMON_INCLUDE) -L $(LEMON_LIBRARY) -lemon lens.cc -o lens$$(python3-config --extension-suffix)
